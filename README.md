Run the extension with Node…

```sh
$ sudo node extension.js
```

…and import the extension into Scratch by shift-clicking the **File** menu, selecting **Import Experimental Extension**, and choosing `extension.json` in the file dialog.

![Shift-click File menu with 'Import Experimental Extension' highlighted"](http://scratch.mit.edu/internalapi/asset/0a21c83d286a59ad7f0408fd8296eb82.png/get/)

the block will appear in the **More Blocks** category. 

![Image of the 'say hello' block in the 'More Blocks' palette](http://scratch.mit.edu/internalapi/asset/65a2b074d530f905ccb840853d06842d.png/get/)



That's it!


Or

Run The Extestion Manulay

'''
https://github.com/redmangospros/AlphaBloxy
'''
